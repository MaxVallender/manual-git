
		gg gg gg    ii      tt
		gg    gg         tt tt tt
		gg gg gg    ii      tt
		      gg    ii      tt
		gg gg gg    ii       tt

--------------------------------------------------------------------------------

# W o r k f l o w s

## How it works

 * **Index** - Cache for create a new commit.
 * **HEAD** - Is the highest commit on top of the branch.
 * **master/main/..** - 

## Help

Following doesn't work under Windows. So try the short version.

	$ man git-blame
	$ git blame --help

Short-help:

	> git blame -h

## Start / Initialize git-Repository

	$ git init
	$ git config user.name "First Last"
	$ git config user.email First.Last@domain.tld
	$ git commit --allow-empty -m "Initial commit"
	$ gitk &
	$ git add file
	$ git commit -m "Message"
	$ git remote add origin https://username@url.tld/Repository.git
	$ git push -u origin master

## Push code to different location (afterwards)
	
	$ git remote add GitHub https://UserName:Token@github.com/UserName/Repository.git
	$ git push GitHub master

## Merging


## Fake Merge

	$ git checkout master
	$ git merge DevOps/master -s ours --no-ff --no-commit
	$ git checkout DevOps/master pathfolder_something.cs
	$ git commit -m "Message"

## Collaborate


## Loop

Run tot all directories in this folder and show 'git status'

	$ for i in *; do (cd $i; git status;) done;	

## Verloren gegangene Commits

	$ git log --reflog				Funktioniert gut
							auch?: git reflog |  awk '{ print $1 }' | xargs gitk

	$ git fsck --unreachable			Unübersichtlich


## Verloren gegangene Dateien

	$ git fsck --lost-found
	$ cd .git/lost-found/other/



	Aufräumen?: git commit --amend

--------------------------------------------------------------------------------

# Q u i c k   R e f e r e n c e

## git add

	$ git add file1 file2 files*

	$ git add -u					--update | Add all changes of files in index

	$ git add -A					--all | Add all changes

## git am

	$ git am 0001-Last-Commit-Message.patch

	$ git am --abort

## git branch

	$ git branch
	* master

	$ git branch -a
	* master
	  remotes/origin/HEAD -> origin/master
	  remotes/origin/master

	$ git branch -vv
	  otherBranch       fd964e1 [origin/otherBranch] Comment from last commit
	  master            676a69e [origin/master] Comment from last commit	

	$ git branch newBranch

	$ git branch branch commitHash			Set branch to commit (vielleicht auch mit -f)
	$ git branch newBranch commitHash

	-> $ git checkout -b newBranch

	$ git branch -d deleteBranch
	$ git branch -rd origin/deleteBranch

	$ git branch -m oldBranchName newBranchName
	-> git branch -u origin/newName newName TODO ----------------------------

## git checkout

	$ git checkout branch
	$ git checkout branch file

	$ git checkout HEAD~1 file

	$ git checkout -b newBranch

	$ git checkout -b branch origin/branch
	

Special version of a file.
		
	-> $ git show branch:path/file > output.file

## git clone

	$ git clone https://url.tld/Repository.git

	$ git clone https://url.tld/Repository.git newFolder

## git commit

	$ git commit -m "Message"
	$ git commit -m "Message" file

	$ git commit --allow-empty -m "Initial commit"

## git config

	--global

	$ git config user.name "First Last"

	$ git config user.email First.Last@domain.tld

	$ git config diff.tool

	$ git config merge.tool

	$ git config core.autocrlf

## git diff

	$ git diff

	$ git difftool

## git fetch

	$ git fetch
	$ git fetch origin

	$ git fetch --all

## git format-patch

	$ git format-patch HEAD~1
	0001-Last-Commit-Message.patch

	-> $ git am 0001-Last-Commit-Message.patch

## git init

	$ git init

	$ git init --bare

Transform a normal git repository to server git. Must allow access from outside.

	-> $ git config receive.denycurrentbranch ignore

## git log

	$ git log

	-> $ git shortlog

## git merge

	$ git merge otherBranch
	$ git merge origin/otherBranch

	$ git merge --no-commit

	$ git merge --continue

	$ git merge --abort

## git pull

	$ git pull
	$ git pull origin
	$ git pull origin master

	$ git pull origin --all

	-f

## git push

	$ git push
	$ git push origin
	$ git push origin branch

	$ git push --set-upstream origin localBranch
	$ git push -u origin localBranch

	$ git push origin --all				Don't do.

	$ git push origin --delete branchName

	-f	->	--force-with-lease

## git remote

	$ git remote -v

	$ git remote add newOrigin url

	$ git remote set-url origin https://USERNAME:SECRET@bitbucket.org/USERNAME/PROJECT.git
	$ git remote add newOrigin https://USERNAME:SECRET@github.com/USERNAME/PROJECT.git

	$ git remote rename oldOrigin newOrigin

	$ git remote remove oldOrigin

## git reset

	$ git reset --hard commitHash

	$ git reset --mixed commitHash

	$ git reset --soft commitHash

## git revert

	$ git revert commit

## git shortlog

	$ git shortlog

## git show
	
	$ git show branch:path/file > output.file	Special version of a file.

## git status

	$ git status

## git tag

	$ git tag -l

	$ git tag -a tagName -m "Message"
	$ git tag -a tagName commitHash

	-> $ git push --tags

## gitk

	$ gitk

	$ gitk --date-order

	$ gitk --date-order --all

	$ gitk --branches=maste*


## .gitignore

Some pre-configured .gitignore are here
https://github.com/github/gitignore


--------------------------------------------------------------------------------

# Einstellunge beim Repository-Provider

## BitBucket.org

Personal Settings -> Access Managment -> App passwords

## GitHub.com

Settings -> Developer settings -> Personal access tokens


---


Es gibt Git-Strategien
	Ob zB ours/theirs übernommen werden soll. Jedoch halfen sie mir alle nicht. Ich bin auch auf entsprechende Einträge bei Stackoverflow gestoßen.

Diese habe nichts mit Merge/Rebase zu tun!


Gemacht:
	($ git checkout master)

	$ git merge --no-commit featureBranch

edit conflict manually
	$ cp conflict.file_merge conflict.file

	$ git add conflict.file
	$ git checkout featureBranch other.files
	$ git add other.files
	$ git merge --continue

auch wichtig:
	$ git merge --abort


-----

nachher reinigen
	$ git fetch origin --prune


	git remote set-url origin url


---


